# hep-ml

[![Binder](https://binder.cern.ch/badge_logo.svg)](https://binder.cern.ch/v2/gh/https%3A%2F%2Fgitlab.cern.ch%2Fbinder%2Frepos%2Fhep-ml/master)

A base container for HEP machine learning purposes on GPUs. 
