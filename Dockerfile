FROM tensorflow/tensorflow:latest-gpu-py3

## ensure locale is set during build
ENV LANG C.UTF-8

ARG DEBIAN_FRONTEND=noninteractive

RUN pip3 install --no-cache keras && \
    pip3 install --no-cache uproot && \
    pip3 install --no-cache jupyter && \
    pip3 install --no-cache jupyterhub==1.0.0 && \
    pip3 install --no-cache jupyterlab==1.1.4 && \
    pip3 install --no-cache matplotlib && \
	pip3 install --no-cache notebook==6.0.1 && \
    pip3 install --no-cache seaborn && \
    pip3 install --no-cache hep_ml && \
    pip3 install --no-cache sklearn && \
    pip3 install --no-cache tables && \
    pip3 install --no-cache papermill pydot Pillow
    

RUN apt-get update && \
    apt-get install -y git debconf-utils && \
    echo "krb5-config krb5-config/add_servers_realm string CERN.CH" | debconf-set-selections && \
    echo "krb5-config krb5-config/default_realm string CERN.CH" | debconf-set-selections && \
    apt-get install -y krb5-user && \
    apt-get install -y vim less screen graphviz python3-tk wget && \
    apt-get install -y jq tree hdf5-tools bash-completion && \
    apt-get install -y nvidia-cuda-toolkit && \
    apt-get install -y gfortran

ARG NB_USER=jovyan
ARG NB_UID=1000
ENV USER ${NB_USER}
ENV HOME /home/${NB_USER}
ENV SHELL bash

RUN adduser --uid ${NB_UID} ${NB_USER} && passwd -d ${NB_USER}

WORKDIR ${HOME}
USER ${USER}

RUN ln -s /eos ${HOME}/eos
